import java.text.DecimalFormat;
import java.util.Random;

public class EventSale extends Event{   // Класс-наследник для события "Покупка"
    private String productName;  // товар
    private double productPrice; // цена товара в USD
    private final String priceFormat = "00.00";

    public EventSale(){
        productName = "Product" + new Random().nextInt(100);
        DecimalFormat df = new DecimalFormat(priceFormat);
        productPrice = Double.parseDouble(df.format(new Random().nextDouble()*10).replace(',','.'));
    }

    public String getProductName() {
        return productName;
    }

    public double getProductPrice() {
        return productPrice;
    }

    @Override
    public String toString() {
        return super.getUUUID() + ", " + super.getDate() + ", " + productName + ", " + productPrice;
    }
}