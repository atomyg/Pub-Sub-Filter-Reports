import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimerTask;

public class ReportBuilderSales extends TimerTask {

    @Override
    public void run() {
        SimpleDateFormat sdpReports = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss"); //формат даты для записи в БД SQL
        String fileName = App.reportsPath.concat("SalesReport_").concat(sdpReports.format(new Date()).concat(".csv"));
        String sqlQuery;
        ResultSet resultSet;
        StringBuilder sb = new StringBuilder();
        try (Statement statement = App.connection.createStatement()) {
            sqlQuery = "SELECT product_name, SUM(product_price), AVG(product_price) FROM sales " +
                    "WHERE sales.date > now() - interval '1 hour' GROUP BY product_name ORDER BY product_name";
            resultSet = statement.executeQuery(sqlQuery);
            String productName;
            double avg, sum;
            try (PrintWriter writer = new PrintWriter(new FileOutputStream(fileName),true, Charset.forName("windows-1251"))) {
                sb.append("Товар").append(";");
                sb.append("Средний чек").append(";");
                sb.append("Сумма покупок").append(System.lineSeparator());
                writer.append(sb);
                while (resultSet.next()) {
                    productName = resultSet.getString("product_name").trim();
                    avg = resultSet.getDouble("avg");
                    sum = resultSet.getDouble("sum");
                    sb.setLength(0);
                    sb.append(productName).append(";");
                    sb.append(String.valueOf(avg).replace('.', ',')).append(";");
                    sb.append(String.valueOf(sum).replace('.', ',')).append(System.lineSeparator());
                    writer.append(sb);
                }
            } catch (IOException e) {
                System.out.println("Building Sales Report - FAILED writer");
                return;
            }
        } catch (SQLException e) {
            System.out.println("Building Sales Report - FAILED statement");
            return;
        }
        System.out.println("Building Sales Report - OK");
    }
}