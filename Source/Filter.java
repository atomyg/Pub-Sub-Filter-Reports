import java.util.Queue;

public class Filter extends Thread{
    private static final Queue<Event> queue = App.queue;

    @Override
    public void run() {
        Event event;
        while(!isInterrupted()){
            synchronized (queue){
                if (!queue.isEmpty()) {
                    event = queue.poll();
                    if (event instanceof EventSale) {
                        while (true) if (App.salesQueue.offer((EventSale)event)) break;
                    }else if (event instanceof EventLevel) {
                        while (true) if (App.levelsQueue.offer((EventLevel)event)) break;
                    }else if (event instanceof EventSession) {
                        while (true) if (App.sessionsQueue.offer((EventSession)event)) break;
                    }
                }
            }
        }
    }
}