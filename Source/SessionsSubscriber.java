import java.sql.SQLException;
import java.sql.Statement;
import java.util.Queue;

public class SessionsSubscriber extends Subscriber{
    private static final Queue<EventSession> queue = App.sessionsQueue;

    @Override
    public void run() {
        EventSession event;
        String sqlQuery;
        while(!isInterrupted()){
            synchronized (queue){
                if (!queue.isEmpty()) {
                    try (Statement statement = App.connection.createStatement()){
                        event = queue.poll();
                        sqlQuery = "INSERT INTO sessions (id, user_uuid, date, time) " +
                                "VALUES (default, '" + event.getUUUID() + "', '" + event.getDate() + "', '" + event.getSessionTime() + "');";
                        statement.executeUpdate(sqlQuery);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}