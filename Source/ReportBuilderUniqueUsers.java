import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimerTask;

public class ReportBuilderUniqueUsers extends TimerTask {

    @Override
    public void run() {
        SimpleDateFormat sdpReports = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss"); //формат даты для записи в БД SQL
        String fileName = App.reportsPath.concat("UniqueUsersReport_").concat(sdpReports.format(new Date()).concat(".csv"));
        String sqlQuery;
        ResultSet resultSet;
        StringBuilder sb = new StringBuilder();
        try (Statement statement = App.connection.createStatement()) {
            sqlQuery = "SELECT COUNT(users_count) FROM " +
                    "(SELECT COUNT(user_uuid) AS users_count " +
                    "FROM sessions WHERE sessions.date > now() - interval '5 minute' GROUP BY user_uuid) " +
                    "AS usrs WHERE users_count = 1";
            resultSet = statement.executeQuery(sqlQuery);
            String count;
            try (PrintWriter writer = new PrintWriter(new FileOutputStream(fileName),true, Charset.forName("windows-1251"))) {
                sb.append("Кол-во").append(System.lineSeparator());
                writer.append(sb);
                while (resultSet.next()) {
                    count = resultSet.getString("count");
                    sb.setLength(0);
                    sb.append(String.valueOf(count)).append(System.lineSeparator());
                    writer.append(sb);
                }
            } catch (IOException e) {
                System.out.println("Building UniqueUsers Report - FAILED writer");
                return;
            }
        } catch (SQLException e) {
            System.out.println("Building UniqueUsers Report - FAILED statement");
            return;
        }
        System.out.println("Building UniqueUsers Report - OK");
    }
}