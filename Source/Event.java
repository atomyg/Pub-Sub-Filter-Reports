import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

// Класс-шаблон для всех 3-х типов событий
public class Event {
    private StringBuilder uuid;   // Cтроковый идентификатор пользователя в 64 символа
    private String date;          // Дата и время события
    private final static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); // ISO 8601

    public Event(){
        String uuid1 = UUID.randomUUID().toString();
        String uuid2 = UUID.randomUUID().toString();
        String result = uuid1.concat("-").concat(uuid2);
        uuid = new StringBuilder(64).append(result);
        uuid.setLength(64);
        date = sdf.format(new Date());
    }

    public StringBuilder getUUUID() {
        return uuid;
    }

    public String getDate() {
        return date;
    }
}