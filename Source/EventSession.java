import java.util.Random;

public class EventSession extends Event{    // Класс-наследник для события "Завершение сессии"
    private int sessionTime;   // длительность сессии в секундах

    public EventSession(){
        sessionTime = Math.abs(new Random().nextInt())/1000;
    }

    public int getSessionTime() {
        return sessionTime;
    }

    @Override
    public String toString() {
        return super.getUUUID() + ", " + super.getDate() + ", " + sessionTime;
    }
}
