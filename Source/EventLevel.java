import java.util.Random;

public class EventLevel extends Event{  // Класс-наследник для события "Достижение уровня"
    int level;

    public EventLevel(){
        this.level = new Random().nextInt(100);
    }

    public int getLevel() {
        return level;
    }

    @Override
    public String toString() {
        return super.getUUUID() + ", " + super.getDate() + ", " + level;
    }
}