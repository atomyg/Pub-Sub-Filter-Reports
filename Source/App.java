import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;

public class App {
    static volatile Queue<Event> queue = new ConcurrentLinkedQueue<>(); // общая очередь всех событий от всех издателей
    static volatile Queue<EventSale> salesQueue = new ConcurrentLinkedQueue<>(); // очередь событий "Продажа"
    static volatile Queue<EventLevel> levelsQueue = new ConcurrentLinkedQueue<>(); // очередь событий "Повышение уровня"
    static volatile Queue<EventSession> sessionsQueue = new ConcurrentLinkedQueue<>(); // очередь событий "Сессия"
    private static List<Publisher> publishers = new ArrayList<>();  //список всех издателей (игр)
    private static List<Subscriber> subscribers = new ArrayList<>();    // список подписчиков на данные от игр
    private static List<Filter> filters = new ArrayList<>();    //список потоков фильтрации
    private static final int publisherThreadsCount = 6; // кол-во нитей для генерации событий
    private static final int filterThreadsCount = 2; // кол-во нитей для фильтрации событий
    private static final int loadTimePeriod = 1; // кол-во секунд, для вывода в консоль состояния очередей
    static final String reportsPath = ("C:\\1\\");  //путь к папке для сохранения отчетов
    private static final int reportsPeriod = 1;  //период в минутах для создания отчетов
    static Connection connection;

    static {
        try {
            Class.forName("org.postgresql.Driver");
            System.out.println("Connecting to PostgreSQL JDBC Driver - OK");
            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/Dev2dev", "postgres", "iph3gSQL");
            System.out.println("Connecting to SQL database - OK");
        } catch (ClassNotFoundException e) {
            System.out.println("Connecting to PostgreSQL JDBC Driver - FAILED");
            System.exit(0);
        } catch (SQLException e) {
            System.out.println("Connecting to SQL database - FAILED");
            System.exit(0);
        }
    }

    public static void main(String[] args) {
        createTables();       // Создание таблиц в БД, если не созданы
        startSubscribers();   // Запуск получения событий (подписчиками) и запись в БД в 3 таблицы
        startFiltering();     // Запуск фильтрации событий по 3-м типам
        startPublishers();    // Запуск генерации событий 3-х типов (издателями)
        runReportsBuilding(); // Запуск периодического создания отчетов
        runQueueMonitoring(); // Запуск лога в консоли
    }

    private static void createTables(){
        try {
            System.out.println("Creating tables in database:");
            Statement statement = connection.createStatement();
            String sqlQuery = "CREATE TABLE IF NOT EXISTS sales " +
                "(id            serial NOT NULL PRIMARY KEY," +
                " user_uuid     character(64) NOT NULL," +
                " date          timestamp without time zone NOT NULL," +
                " product_name  character(64) NOT NULL," +
                " product_price double precision NOT NULL)";
            statement.executeUpdate(sqlQuery);
            System.out.println("\t table 'sales' - OK");
            sqlQuery = "CREATE TABLE IF NOT EXISTS levels " +
                    "(id            serial NOT NULL PRIMARY KEY," +
                    " user_uuid     character(64) NOT NULL," +
                    " date          timestamp without time zone NOT NULL," +
                    " level         smallint NOT NULL)";
            statement.executeUpdate(sqlQuery);
            System.out.println("\t table 'levels' - OK");
            sqlQuery = "CREATE TABLE IF NOT EXISTS sessions " +
                    "(id            serial NOT NULL PRIMARY KEY," +
                    " user_uuid     character(64) NOT NULL," +
                    " date          timestamp without time zone NOT NULL," +
                    " time          integer NOT NULL)";
            statement.executeUpdate(sqlQuery);
            System.out.println("\t table 'sessions' - OK");
        } catch (SQLException e) {
            System.out.println("FAILED");
            e.printStackTrace();
            System.exit(0);
        }
    }

    private static void startPublishers(){
        Publisher game;
        for (int i = 1; i <= publisherThreadsCount; i++) {
            game = new Publisher();
            publishers.add(game);
            game.start();
        }
    }

    private static void startFiltering(){
        Filter filter;
        for (int i = 1; i <= filterThreadsCount; i++) {
            filter = new Filter();
            filters.add(filter);
            filter.start();
        }
    }

    private static void startSubscribers(){
        Subscriber salesSubscriber = new SalesSubscriber(); // подписчик только на события "Продажа"
        Subscriber levelsSubscriber = new LevelsSubscriber();   // подписчик только на события "Повышения уровня"
        Subscriber sessionsSubscriber = new SessionsSubscriber();   // подписчик только на события "Сессия"
        salesSubscriber.start();
        levelsSubscriber.start();
        sessionsSubscriber.start();
        subscribers.add(salesSubscriber);
        subscribers.add(levelsSubscriber);
        subscribers.add(sessionsSubscriber);
    }

    protected static void interruptAllThreads(){
        for (Publisher p : publishers)
            p.interrupt();
        for (Filter f : filters)
            f.interrupt();
        for (Subscriber s : subscribers)
            s.interrupt();
    }

    private static void runReportsBuilding(){
        int delay = reportsPeriod*60*1000;
        Timer timer = new Timer();
        timer.schedule(new ReportBuilderSales(), delay, delay);
        timer.schedule(new ReportBuilderUniqueUsers(), delay, delay);
        timer.schedule(new ReportBuilderLevelUsers(), delay, delay);
    }

    private static void runQueueMonitoring() {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        StringBuilder stringBuilder = new StringBuilder();
        int temp;
        int count = 0;
        while (true) {
            stringBuilder.setLength(0);
            try {
                Thread.sleep(loadTimePeriod * 1000);
                temp = count;
                count = Publisher.countAllEvents.get();
                stringBuilder.append(sdf.format(new Date())).append(" ");
                stringBuilder.append("create = ").append(count).append(" (+");
                stringBuilder.append(count - temp).append("); ");
                stringBuilder.append("queue = ").append(queue.size()).append("; ");
                stringBuilder.append("sales = ").append(salesQueue.size()).append("; ");
                stringBuilder.append("levels = ").append(levelsQueue.size()).append("; ");
                stringBuilder.append("sessions = ").append(sessionsQueue.size()).append("; ");
                System.out.println(stringBuilder);
            } catch (InterruptedException e) {
                System.out.println("Main thread interrupted");
                e.printStackTrace();
                interruptAllThreads();
            }
        }
    }
}