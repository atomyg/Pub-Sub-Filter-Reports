import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

// генерация 1 события случайного типа и добавление его в общую очередь (канал событий)
public class Publisher extends Thread {
    private Event event;
    private int num;
    public static AtomicInteger countAllEvents = new AtomicInteger(0);

    @Override
    public void run() {
        while (true) {
            num = new Random().nextInt();
            if (num % 3 == 0) {
                event = new EventSale();
            } else if (num % 2 == 0) {
                event = new EventLevel();
            } else event = new EventSession();
            App.queue.offer(event);
            countAllEvents.incrementAndGet();
            try {
                sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}