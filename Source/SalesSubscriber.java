import java.sql.SQLException;
import java.sql.Statement;
import java.util.Queue;

public class SalesSubscriber extends Subscriber{
    private static final Queue<EventSale> queue = App.salesQueue;

    @Override
    public void run() {
        EventSale event;
        String sqlQuery;
        while(!isInterrupted()){
            synchronized (queue){
                if (!queue.isEmpty()) {
                    try (Statement statement = App.connection.createStatement()){
                        event = queue.poll();
                        sqlQuery = "INSERT INTO sales (user_uuid, date, product_name, product_price) " +
                                "VALUES ('" + event.getUUUID() + "', '" + event.getDate() + "', '" +
                                event.getProductName()+ "', '" + event.getProductPrice() + "');";
                        statement.executeUpdate(sqlQuery);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}