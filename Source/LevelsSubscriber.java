import java.sql.SQLException;
import java.sql.Statement;
import java.util.Queue;

public class LevelsSubscriber extends Subscriber{
    private static final Queue<EventLevel> queue = App.levelsQueue;

    @Override
    public void run() {
        EventLevel event;
        String sqlQuery;
        while(!isInterrupted()){
            synchronized (queue){
                if (!queue.isEmpty()) {
                    try (Statement statement = App.connection.createStatement()){
                        event = queue.poll();
                        sqlQuery = "INSERT INTO levels (user_uuid, date, level) " +
                            "VALUES ('" + event.getUUUID() + "', '" + event.getDate() + "', '" + event.getLevel()+ "');";
                        statement.executeUpdate(sqlQuery);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}