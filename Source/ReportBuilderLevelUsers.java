import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimerTask;

public class ReportBuilderLevelUsers extends TimerTask {

    @Override
    public void run() {
        SimpleDateFormat sdpReports = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss"); //формат даты для записи в БД SQL
        String fileName = App.reportsPath.concat("LevelUsersReport_").concat(sdpReports.format(new Date()).concat(".csv"));
        String sqlQuery;
        ResultSet resultSet;
        StringBuilder sb = new StringBuilder();
        try (Statement statement = App.connection.createStatement()) {
            sqlQuery = "SELECT levels.level, COUNT(user_uuid) FROM levels " +
                        "WHERE levels.date > now() - interval '1 hour' " +
                        "GROUP BY levels.level ORDER BY levels.level DESC";
            resultSet = statement.executeQuery(sqlQuery);
            String level;
            int count, sum = 0;
            try (PrintWriter writer = new PrintWriter(new FileOutputStream(fileName),true, Charset.forName("windows-1251"))) {
                sb.append("Уровень").append(";");
                sb.append("Кол-во пользователей").append(System.lineSeparator());
                writer.append(sb);
                while (resultSet.next()) {
                    level = resultSet.getString("level");
                    count = resultSet.getInt("count");
                    sum += count;
                    sb.setLength(0);
                    sb.append(level).append(";");
                    sb.append(sum).append(System.lineSeparator());
                    writer.append(sb);
                }
            } catch (IOException e) {
                System.out.println("Building Levels Report - FAILED writer");
                return;
            }
        } catch (SQLException e) {
            System.out.println("Building Levels Report - FAILED statement");
            return;
        }
        System.out.println("Building Levels Report - OK");
    }
}